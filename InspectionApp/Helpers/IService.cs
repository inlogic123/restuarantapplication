using System.Security.Claims;
using System.Threading.Tasks;

namespace InspectionApp.Helpers
{
    public interface IService
    {
         Task<bool> ValidateUser(int id, ClaimsPrincipal user);
    }
}