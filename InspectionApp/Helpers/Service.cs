using System.Security.Claims;
using System.Threading.Tasks;

namespace InspectionApp.Helpers
{
    public class Service : IService
    {
        public async Task<bool> ValidateUser(int id, ClaimsPrincipal user)
        {
            return id == int.Parse(user.FindFirst(ClaimTypes.NameIdentifier).Value);
        }
    }
}