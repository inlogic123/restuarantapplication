using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Models;

namespace InspectionApp.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<UserForRegisterDTO, User>();
            CreateMap<User, UserForDetailsDTO>();
            CreateMap<User, UserForListDTO>();
            CreateMap<EntityToCreate, Entity>();
            CreateMap<Entity, EntitiesForListDto>();
            CreateMap<Contact, ContactForEntityListDto>();
            CreateMap<User, UserForEntityListDto>();
            CreateMap<OwnerToCreate, Contact>();
            CreateMap<EntityBranchToCreateDto, EntityBranch>();
            CreateMap<CompanyScopeOfWorkToCreateDto, CompanyScopeOfWork>();
            CreateMap<ContactToCreateDto, Contact>();
            CreateMap<ContactEntityBranchToCreateDto, ContactEntityBranch>();
            CreateMap<EntityBranchAddressToCreate, EntityBranchAddress>();
            CreateMap<AddressToCreateDto, Address>();
            CreateMap<EntityToUpdateDto, Entity>();
            CreateMap<OwnerToUpdate, Contact>();
            CreateMap<EntityBranchToUpdateDto, EntityBranch>();
            CreateMap<EntityBranchAddressToUpdate, EntityBranchAddress>();
            CreateMap<AddressToUpdateDto, Address>();
            CreateMap<ApplicationToCreateDto, InspectionApplication>();
            CreateMap<ApplicationToUpdate, InspectionApplication>();
            CreateMap<InspectionApplication, ApplicationsToList>();
            CreateMap<InspectionApplication, ApplicationForDetailsDto>();
            CreateMap<User, UserShortDto>();
            CreateMap<LookupDetail, LookupDto>();
            CreateMap<EntityBranch, EntityBranchDto>();
            CreateMap<WorkFlowToCreateDto, WorkFlow>();
            CreateMap<TaskToCreateDto, Task>();
            CreateMap<TaskActionToCreateDto, TaskAction>();
            CreateMap<WorkflowToUpdateDto, WorkFlow>();
            CreateMap<TaskToUpdateDto, Task>();
            CreateMap<TaskActionToUpdateDto, TaskAction>();
            CreateMap<AuditListForCreateDto, AuditList>();
            CreateMap<AuditList, AuditListsToViewDto>();
            CreateMap<StandardForCreateDto, Standard>();
            CreateMap<Standard, StandardToViewDto>();
            CreateMap<Standard, StandardListToViewDto>();
            CreateMap<EvaluationToCreateDto, Evaluation>();
            CreateMap<Evaluation, EvaluationToViewDto>();
            CreateMap<Evaluation, EvaluationListToViewDto>();
            CreateMap<Task, TasksToListDto>();
            CreateMap<WorkFlow, WorkflowToViewDto>();
            CreateMap<User, UserToViewDto>();
            CreateMap<TaskAction, TaskActionToViewDto>();
            CreateMap<EmployeeAreaScopeOfWork, EmployeeAreaScopeOfWorkDto>();
            CreateMap<Entity, EntityToViewDto>();
            CreateMap<EntityBranch, EntityBranchToViewDto>();
            CreateMap<CompanyScopeOfWork, CompanyScopeOfWorkToViewDto>();
            CreateMap<ScopeOfWork, ScopeOfWorkToViewDto>();
            
        }
    }
}