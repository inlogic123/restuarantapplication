namespace InspectionApp.Enums
{
    public enum LookupFilter
    {
        None = 1,
        SpecificLookup = 2,
        SpecificLookupAndParent = 3
    }
}