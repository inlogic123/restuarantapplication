namespace InspectionApp.Enums
{
    public enum Languages
    {
        Arabic = 1,
        English = 2,
        General = 3
    }
}