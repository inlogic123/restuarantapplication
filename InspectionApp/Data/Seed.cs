using System.Collections.Generic;
using System.Linq;
using InspectionApp.Models;
using Microsoft.AspNetCore.Identity;

namespace InspectionApp.Data
{
    public class Seed
    {
        public static void SeedRoles(RoleManager<Role> roleManager)
        {
            if (!roleManager.Roles.Any())
            {
                var roles = new List<Role>
                {
                    new Role{Name = "Admin"},
                    new Role{Name = "Inspector"},
                    new Role{Name = "User"},
                    new Role{Name = "Manager"}
                };

                foreach (var role in roles)
                {
                    roleManager.CreateAsync(role).Wait();
                }
            }
            
        }
    }
}