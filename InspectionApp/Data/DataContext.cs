using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using InspectionApp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Task = InspectionApp.Models.Task;

namespace InspectionApp.Data
{
    public class DataContext : IdentityDbContext<User, Role, int, IdentityUserClaim<int>,
                    UserRole, IdentityUserLogin<int>, IdentityRoleClaim<int>, IdentityUserToken<int>>
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Lookup> Lookups { get; set; }
        public DbSet<LookupDetail> LookupDetails { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Certification> Certifications { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<EntityBranch> EntityBranches { get; set; }
        public DbSet<InspectionApplication> InspectionApplications { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskAction> TaskActions { get; set; }
        public DbSet<ScheduleTask> ScheduleTasks { get; set; }
        public DbSet<ScopeOfWork> ScopeOfWorks { get; set; }
        public DbSet<CompanyScopeOfWork> CompanyScopeOfWorks { get; set; }
        public DbSet<EmployeeAreaScopeOfWork> EmployeeAreaScopeOfWorks { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<UserTask> UserTasks { get; set; }
        public DbSet<ScopeStandard> ScopeStandards { get; set; }
        public DbSet<Evaluation> Evaluations { get; set; }
        public DbSet<AuditList> AuditLists { get; set; }
        public DbSet<AuditListItem> AuditListItems { get; set; }
        public DbSet<WorkFlow> WorkFlows { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // builder.Entity<Entity>().HasQueryFilter(e => e.IsActive == true);

            // builder.Entity<User>(user =>
            // {
            //     user.HasOne(u => u.CreatedBy)
            //         .WithMany(c => c.UsersCreated)
            //         .HasForeignKey(r => r.CreatedById)
            //         .IsRequired();

            //     user.HasOne(u => u.ModifiedBy)
            //         .WithMany(c => c.UsersModified)
            //         .HasForeignKey(r => r.ModifiedById)
            //         .IsRequired();
            // });

            builder.Entity<UserRole>(userRole =>
                {
                    userRole.HasKey(ur => new { ur.UserId, ur.RoleId });
                    userRole.HasOne(ur => ur.User)
                        .WithMany(u => u.UserRoles)
                        .HasForeignKey(r => r.UserId)
                        .IsRequired();

                    userRole.HasOne(ur => ur.Role)
                        .WithMany(u => u.UserRoles)
                        .HasForeignKey(r => r.RoleId)
                        .IsRequired();
                });

            builder.Entity<ScopeStandard>(scopeStandard =>
            {
                scopeStandard.HasOne(ss => ss.ScopeOfWork)
                    .WithMany(sw => sw.ScopeStandards)
                    .HasForeignKey(ss => ss.ScopeOfWorkId)
                    .IsRequired();

                scopeStandard.HasOne(ss => ss.Standard)
                    .WithMany(st => st.ScopeStandards)
                    .HasForeignKey(ss => ss.StandardId)
                    .IsRequired();
            });


            builder.Entity<ContactEntityBranch>(ceb =>
            {
                ceb.HasOne(ceb => ceb.Contact)
                    .WithMany(c => c.ContactEntityBranches)
                    .HasForeignKey(ceb => ceb.ContactId)
                    .IsRequired();

                ceb.HasOne(ceb => ceb.EntityBranch)
                    .WithMany(c => c.ContactEntityBranches)
                    .HasForeignKey(ceb => ceb.EntityBranchId)
                    .IsRequired();
            });

            // builder.Entity<EntityBranchAddress>(
            //     eba =>
            //     {
            //         eba.HasOne(e => e.Address)
            //             .WithMany(a => a.EntityBranchAddress)
            //             .HasForeignKey(e => e.AddressId);

            //         eba.HasOne(e => e.EntityBranch)
            //             .WithMany(a => a.EntityBranchAddress)
            //             .HasForeignKey(e => e.EntityBranchId);
            //     }
            // );

            builder.Entity<Entity>(entity =>
            {
                entity.HasOne(e => e.Owner)
                .WithMany(o => o.Entities)
                .HasForeignKey(e => e.OwnerId)
                .IsRequired();
            });

            builder.Entity<CompanyScopeOfWork>(companyScopes =>
            {
                companyScopes.HasOne(cs => cs.EntityBranch)
                    .WithMany(e => e.CompanyScopeOfWorks)
                    .HasForeignKey(cs => cs.EntityBranchId)
                    .IsRequired();

                companyScopes.HasOne(cs => cs.ScopeOfWork)
                    .WithMany(e => e.CompanyScopeOfWorks)
                    .HasForeignKey(cs => cs.ScopeOfWorkId)
                    .IsRequired();
            });

            builder.Entity<EmployeeAreaScopeOfWork>(employeeAreaScopes =>
            {
                employeeAreaScopes.HasOne(eas => eas.Employee)
                    .WithMany(e => e.EmployeeAreaScopeOfWorks)
                    .HasForeignKey(eas => eas.EmployeeId)
                    .IsRequired();

                employeeAreaScopes.HasOne(eas => eas.Area)
                    .WithMany(e => e.EmployeeAreaScopeOfWorks)
                    .HasForeignKey(eas => eas.AreaId)
                    .IsRequired();

                employeeAreaScopes.HasOne(eas => eas.ScopeOfWork)
                    .WithMany(e => e.EmployeeAreaScopeOfWorks)
                    .HasForeignKey(eas => eas.ScopeOfWorkId)
                    .IsRequired();
            });

            builder.Entity<TaskAction>(taskAction =>
            {
                taskAction.HasOne(ta => ta.Task)
                    .WithMany(t => t.TaskActions)
                    .HasForeignKey(ta => ta.TaskId)
                    .IsRequired();
            });

            builder.Entity<UserTask>(userTask =>
            {
                userTask.HasOne(ut => ut.AssignedBy)
                    .WithMany(ab => ab.AssignedTasks)
                    .HasForeignKey(ut => ut.AssignedById)
                    .IsRequired();

                userTask.HasOne(ut => ut.AssignedTo)
                    .WithMany(at => at.UserTasks)
                    .HasForeignKey(ut => ut.AssignedToId)
                    .IsRequired();

                userTask.HasOne(ut => ut.Application)
                    .WithMany(ap => ap.UserTasks)
                    .HasForeignKey(ut => ut.ApplicationId)
                    .IsRequired();
            });
        }
    }
}