using System;
using InspectionApp.Models;

namespace InspectionApp.Repositories
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public User CreatedBy { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}