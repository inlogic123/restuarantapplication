using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InspectionApp.Repositories
{
    public interface IRepository<T> where T : class
    {
        void Create(T entity);
        void Delete(int id);
        void Edit(T entity);
        

        //read side (could be in separate Readonly Generic Repository)
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        //separate method SaveChanges can be helpful when using this pattern with UnitOfWork
        Task<bool> SaveChanges();
    }
}