using System.Threading.Tasks;
using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);
         Task<User> Login(string username, string password);
         Task<bool> UserExists(string username);

         Task<User> GetUser(string username);
    }
}