using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IStandardRepository : IRepository<Standard>
    {
         
    }
}