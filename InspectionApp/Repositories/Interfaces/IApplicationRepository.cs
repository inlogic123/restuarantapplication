using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IApplicationRepository : IRepository<InspectionApplication>
    {
        Task<int> GetUserForTask(int scopeId, int areaId);
        Task<bool> CeateTask(UserTask userTask);
        Task<int> GetFirstTask();
        Task<PagedList<InspectionApplication>> GetAllByRole(string role, int userId, UserParams userParams);
        Task<IEnumerable<Entity>> GetEntitiesByArea(List<int> areas);
        Task<List<int>> GetUserAreaSOW(int userId); 

    }
}