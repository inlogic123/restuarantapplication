using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IEntityRepository : IRepository<Entity>
    {
        Task<IEnumerable<Entity>> GetEntitiesByOwnerId(int id);
        Task<IEnumerable<Entity>> GetEntities(int id);
    }
}