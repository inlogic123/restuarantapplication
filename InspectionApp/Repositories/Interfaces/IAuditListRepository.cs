using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IAuditListRepository : IRepository<AuditList>
    {
         
    }
}