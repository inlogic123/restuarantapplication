using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Enums;
using InspectionApp.Models;

namespace InspectionApp.Repositories.Interfaces
{
    public interface ILookupRepository
    {
        Task<List<Lookup>> GetLookupWithKey(string key, Languages language);
        Task<List<LookupDetail>> GetLookupWithParentId(int parentId);
        Task<List<Lookup>> GetLookupWithParentAndLanguage(int parentId, Languages languageId);
    }
}