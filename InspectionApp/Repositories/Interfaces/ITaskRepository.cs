using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Helpers;
using InspectionApp.Models;
using Task = InspectionApp.Models.Task;

namespace InspectionApp.Repositories.Interfaces
{
    public interface ITaskRepository : IRepository<Task>
    {
        Task<PagedList<Task>> GetUserTasks(int userId, UserParams userParams);
    }
}