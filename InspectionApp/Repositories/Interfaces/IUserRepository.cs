using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Models;
using Task = InspectionApp.Models.Task;

namespace InspectionApp.Repositories.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetUser(int id, bool ignorQueryFilter); 
        Task<IEnumerable<EmployeeAreaScopeOfWork>> GetUserAreaSOW(int id);
    }
}