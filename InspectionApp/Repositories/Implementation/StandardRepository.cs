using InspectionApp.Data;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;

namespace InspectionApp.Repositories.Implementation
{
    public class StandardRepository : Repository<Standard>, IStandardRepository
    {
        private readonly DataContext _context;
        public StandardRepository(DataContext context) : base(context)
        {
            _context = context;
        }
    }
}