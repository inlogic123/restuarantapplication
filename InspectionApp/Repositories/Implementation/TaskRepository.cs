using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InspectionApp.Data;
using InspectionApp.Helpers;
using InspectionApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Task = InspectionApp.Models.Task;

namespace InspectionApp.Repositories.Implementation
{
    public class TaskRepository : Repository<Task>, ITaskRepository
    {
        private readonly DataContext _context;
        public TaskRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<PagedList<Task>> GetUserTasks(int userId, UserParams userParams)
        {
            var tasks = _context.UserTasks.Where(ut => ut.AssignedToId == userId).Select(ut => ut.Task);
            return await PagedList<Task>.CreateAsync(tasks, userParams.PageNumber, userParams.PageSize);
        }
    }
}