using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InspectionApp.Data;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InspectionApp.Repositories.Implementation
{
    public class ApplicationRepository : Repository<InspectionApplication>, IApplicationRepository
    {
        private readonly DataContext _context;
        public ApplicationRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<bool> CeateTask(UserTask userTask)
        {
            var task = await _context.UserTasks.AddAsync(userTask);
            if (task == null)
                return false;

            return true;
        }

        public async Task<PagedList<InspectionApplication>> GetAllByRole(string role, int userId, UserParams userParams)
        {
            var applications = _context.InspectionApplications.AsQueryable();
            if (role == "user")
                applications = applications.Where(a => a.CreatedById == userId);

            return await PagedList<InspectionApplication>.CreateAsync(applications, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<IEnumerable<Entity>> GetEntitiesByArea(List<int> areas)
        {
            var ids = string.Join(',', areas);

            var entities = await _context.Entities.FromSqlRaw($@"select distinct [Entities].Id, [Entities].[Name],[Entities].[CreatedById],[Entities].[IsActive],[Entities].[OwnerId], [Entities].[TradeLicenseNo] from [Entities]
                             inner join [EntityBranches] on [EntityBranches].EntityId = [Entities].Id
                                           inner join [Addresses] on [EntityBranches].AddressId = [Addresses].Id
                                           where [Addresses].AreaId in ({ids}) ").ToListAsync();
            return entities;
        }

        public async Task<int> GetFirstTask()
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(t => t.WorkFlowId == 11);
            return task.Id;
        }

        public async Task<int> GetUserForTask(int scopeId, int areaId)
        {
            var users = await _context.EmployeeAreaScopeOfWorks.Where(eas => eas.AreaId == areaId && eas.ScopeOfWorkId == scopeId).ToListAsync();

            if (users == null)
                return default(int);

            Random random = new Random();
            return users[random.Next(users.Count)].EmployeeId;
        }
        public async Task<List<int>> GetUserAreaSOW(int userId)
        {
            return await _context.EmployeeAreaScopeOfWorks.Where(eas => eas.EmployeeId == userId).Select(eas => eas.AreaId).ToListAsync();
        }

    }
}