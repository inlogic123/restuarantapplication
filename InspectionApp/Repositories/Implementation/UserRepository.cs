using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InspectionApp.Data;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InspectionApp.Repositories.Implementation
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly DataContext _context;
        public UserRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<User> GetUser(int id, bool ignorQueryFilter)
        {
            return await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<EmployeeAreaScopeOfWork>> GetUserAreaSOW(int id)
        {
            return await _context.EmployeeAreaScopeOfWorks.Where(eas => eas.EmployeeId == id).ToListAsync();
        }

        // public async Task<IEnumerable<Models.UserTask>> GetUserTasks(int id)
        // {
        //     return await _context.UserTasks.Where(t => t.AssignedToId == id).ToListAsync();
        // }
    }
}