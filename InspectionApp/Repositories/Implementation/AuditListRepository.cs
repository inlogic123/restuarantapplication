using InspectionApp.Data;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;

namespace InspectionApp.Repositories.Implementation
{
    public class AuditListRepository : Repository<AuditList>, IAuditListRepository
    {
        private readonly DataContext _context;
        public AuditListRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        
    }
}