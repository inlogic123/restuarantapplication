using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InspectionApp.Data;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace InspectionApp.Repositories.Implementation
{
    public class EntityRepository : Repository<Entity>, IEntityRepository
    {
        private readonly DataContext _context;
        public EntityRepository(DataContext context) : base(context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Entity>> GetEntitiesByOwnerId(int id)
        {
            return await _context.Entities.Where(e => e.OwnerId == id).ToListAsync();
        }

        public async Task<IEnumerable<Entity>> GetEntities(int id)
        {
            return await _context.Entities.Where(e => e.CreatedById == id).ToListAsync();
        }
    }
}