using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InspectionApp.Data;
using InspectionApp.Enums;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace InspectionApp.Repositories.Implementation
{
    public class LookupRepository : ILookupRepository
    {
        private readonly DataContext _context;
        private readonly IMemoryCache _cache;
        public LookupRepository(DataContext context, IMemoryCache cache)
        {
            _cache = cache;
            _context = context;
        }   

        public async Task<List<Lookup>> GetLookupWithKey(string key, Languages language)
        {
            return await _context.Lookups.Include(l => l.LookupDetails).Where(l => l.LanguageId == Convert.ToInt32(language) && l.Key == key).ToListAsync();
        }

        public async Task<List<Lookup>> GetLookupWithParentAndLanguage(int parentId, Languages languageId)
        {
            return await _context.Lookups.Include(l => l.LookupDetails.Where(ld => ld.ParentId == parentId)).Where(l => l.LanguageId == Convert.ToInt32(languageId)).ToListAsync();
        }

        public async Task<List<LookupDetail>> GetLookupWithParentId(int parentId)
        {
            return await _context.LookupDetails.Where(ld => ld.ParentId == parentId).ToListAsync();
        }
    }
}