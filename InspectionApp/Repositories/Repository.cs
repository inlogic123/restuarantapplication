using System.Collections.Generic;
using System.Threading.Tasks;
using InspectionApp.Data;
using Microsoft.EntityFrameworkCore;

namespace InspectionApp.Repositories
{
    public class Repository<T> : IRepository<T> where T : class 
    {
        private readonly DataContext _context;
        private DbSet<T> table;

        public Repository(DataContext context)
        {
            _context = context;
            table = _context.Set<T>();
        }

        public void Create(T entity)
        {
            table.Add(entity);
        }

        public void Delete(int id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
        }

        public void Edit(T entity)
        {
            table.Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            // _context.Update(entity);
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await table.ToListAsync();
        }

        public async Task<T> GetById(int id)
        {
            return await table.FindAsync(id);
        }

        public async Task<bool> SaveChanges()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}