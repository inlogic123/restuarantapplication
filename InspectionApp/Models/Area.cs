using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Area
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<EmployeeAreaScopeOfWork> EmployeeAreaScopeOfWorks { get; set; }
    }
}