using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Contact
    {
        // private string fullName;

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public virtual ICollection<Entity> Entities { get; set; }
        public virtual ICollection<ContactEntityBranch> ContactEntityBranches { get; set; }
        public bool IsActive { get; set; }
        
        // public string FullName { get => fullName; set => fullName = FirstName + " " + LastName; }
    }
}