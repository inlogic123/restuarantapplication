namespace InspectionApp.Models
{
    public class ContactEntityBranch
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public virtual Contact Contact { get; set; }
        public int EntityBranchId { get; set; }
        public virtual EntityBranch EntityBranch { get; set; }
    }
}