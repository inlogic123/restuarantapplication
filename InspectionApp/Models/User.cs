using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace InspectionApp.Models
{
    public class User : IdentityUser<int>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public DateTime LastActive { get; set; }
        public string PhotoUrl { get; set; }
        // public int GenderId { get; set; }
        // public virtual LookupDetail Gender { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
         public virtual ICollection<EmployeeAreaScopeOfWork> EmployeeAreaScopeOfWorks { get; set; }
         public virtual ICollection<Entity> Entities { get; set; }
         public virtual ICollection<UserTask> AssignedTasks { get; set; }
         public virtual ICollection<UserTask> UserTasks { get; set; }
    }
}