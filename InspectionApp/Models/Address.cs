using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CountryId { get; set; }
        public virtual LookupDetail Country { get; set; }
        public int CityId { get; set; }
        public virtual LookupDetail City { get; set; }
        public int AreaId { get; set; }
        public virtual Area Area { get; set; }
        public string Street { get; set; }
        public string BuildingNo { get; set; }
        public string MakaniNumber { get; set; }
        public string Long { get; set; }
        public string Lat { get; set; }
        // public virtual ICollection<EntityBranchAddress> EntityBranchAddress { get; set; }
        public bool IsActive { get; set; }            
    }
}