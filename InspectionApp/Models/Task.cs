using System;
using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? WorkFlowId { get; set; }
        public virtual WorkFlow WorkFlow { get; set; }
        public int CreatedById { get; set; }
        public virtual User CreatedBy { get; set; }
        public int? ModifiedById { get; set; }
        public virtual User ModifiedBy { get; set; }
        public virtual ICollection<TaskAction> TaskActions { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
        public bool IsActive { get; set; }
    }
}