namespace InspectionApp.Models
{
    public class ScopeStandard
    {
        public int Id { get; set; }
        public int StandardId { get; set; }
        public virtual Standard Standard { get; set; }
        public int ScopeOfWorkId { get; set; }
        public virtual ScopeOfWork ScopeOfWork { get; set; }
    }
}