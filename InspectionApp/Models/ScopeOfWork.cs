using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class ScopeOfWork
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ScopeStandard> ScopeStandards { get; set; }
        public virtual ICollection<CompanyScopeOfWork> CompanyScopeOfWorks { get; set; }
         public virtual ICollection<EmployeeAreaScopeOfWork> EmployeeAreaScopeOfWorks { get; set; }
         public virtual ICollection<InspectionApplication> Applications { get; set; }

    }
}