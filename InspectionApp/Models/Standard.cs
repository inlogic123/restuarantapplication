using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Standard
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ScopeStandard> ScopeStandards { get; set; }
        public virtual ICollection<AuditList> AuditLists { get; set; }
        public virtual ICollection<Evaluation> Evaluations { get; set; }
    }
}