using InspectionApp.Repositories;

namespace InspectionApp.Models
{
    public class CompanyScopeOfWork
    {
        public int Id { get; set; }
        public int EntityBranchId { get; set; }
        public virtual EntityBranch EntityBranch { get; set; }
        public int ScopeOfWorkId { get; set; }
        public virtual ScopeOfWork ScopeOfWork { get; set; }
    }
}