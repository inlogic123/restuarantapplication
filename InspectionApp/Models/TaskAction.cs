namespace InspectionApp.Models
{
    public class TaskAction
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
        public int? NextTaskId { get; set; }
        public virtual Task NextTask { get; set; }
    }
}