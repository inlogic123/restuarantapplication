namespace InspectionApp.Models
{
    public class UserTask
    {
        public int Id { get; set; }
        public int AssignedToId { get; set; }
        public virtual User AssignedTo { get; set; }
        public int AssignedById { get; set; }
        public virtual User AssignedBy { get; set; }
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
        public int ApplicationId { get; set; }
        public virtual InspectionApplication Application { get; set; }
        public bool IsActive { get; set; }
    }
}