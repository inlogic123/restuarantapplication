namespace InspectionApp.Models
{
    public class EmployeeAreaScopeOfWork
    {
        public int Id { get; set; }
        public int AreaId { get; set; }
        public virtual Area Area { get; set; }
        public int ScopeOfWorkId { get; set; }
        public virtual ScopeOfWork ScopeOfWork { get; set; }
        public int EmployeeId { get; set; }
        public virtual User Employee { get; set; }
    }
}