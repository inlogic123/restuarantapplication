namespace InspectionApp.Models
{
    public class Evaluation
    {
        public int Id { get; set; }
        public string EvaluationNo { get; set; }
        public string EvaluationResult { get; set; }
        public int ApplicationId { get; set; }
        public virtual InspectionApplication Application { get; set; }
        public int StandardId { get; set; }
        public virtual Standard Standard { get; set; }
    }
}