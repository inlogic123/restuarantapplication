using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class WorkFlow
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public virtual ICollection<Task> Tasks { get; set; }
    }
}