using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class EntityBranch
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int EntityId { get; set; }
        public virtual Entity Entity { get; set; }
        // public virtual ICollection<EntityBranchAddress> EntityBranchAddress { get; set; }
        public int AddressId { get; set; }
        public virtual Address Address { get; set; }
        public virtual ICollection<CompanyScopeOfWork> CompanyScopeOfWorks { get; set; }
        public virtual ICollection<ContactEntityBranch> ContactEntityBranches { get; set; }
        public virtual ICollection<ScheduleTask> ScheduledTasks { get; set; }
        public bool IsActive { get; set; }
    }
}