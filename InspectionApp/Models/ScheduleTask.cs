using System;

namespace InspectionApp.Models
{
    public class ScheduleTask
    {
        public int Id { get; set; }
        public DateTime ScheduleDate { get; set; }
        public int EntityBranchId { get; set; }
        public virtual EntityBranch EntityBranch { get; set; }
        public int ScheduledById { get; set; }
        public virtual User ScheduledBy { get; set; }
    }
}