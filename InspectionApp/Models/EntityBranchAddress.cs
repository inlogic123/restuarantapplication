namespace InspectionApp.Models
{
    public class EntityBranchAddress
    {
        public int Id { get; set; }
        public int EntityBranchId { get; set; }
        public virtual EntityBranch EntityBranch { get; set; }
        public int AddressId { get; set; }
        public virtual Address Address { get; set; }
    }
}