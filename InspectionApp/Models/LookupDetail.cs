using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace InspectionApp.Models
{
    public class LookupDetail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LookupId { get; set; }
        public virtual Lookup Lookup { get; set; }
        public int? ParentId { get; set; }
        public virtual LookupDetail Parent { get; set; }
    }
}