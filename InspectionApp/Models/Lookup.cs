using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Lookup
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }
        public int? ParentId { get; set; }
        public virtual Lookup Parent { get; set; }
        public virtual ICollection<LookupDetail> LookupDetails { get; set; }
    }
}