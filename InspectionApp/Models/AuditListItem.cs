namespace InspectionApp.Models
{
    public class AuditListItem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int AuditListId { get; set; }
        public virtual AuditList AuditList { get; set; }
    }
}