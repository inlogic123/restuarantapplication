namespace InspectionApp.Models
{
    public class Certification
    {
        public int Id { get; set; }
        public string CertificationNo { get; set; }
        public virtual InspectionApplication Application { get; set; }
        public bool IsActive { get; set; }
    }
}