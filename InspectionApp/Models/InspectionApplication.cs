using System;
using System.Collections.Generic;
using InspectionApp.Models;
using InspectionApp.Repositories;

namespace InspectionApp.Models
{
    public class InspectionApplication
    {
        public int Id { get; set; }
        public string ApplicationNo { get; set; }
        public int? ApplicationStatusId { get; set; }
        public virtual LookupDetail ApplicationStatus { get; set; }
        public int EntityBranchId { get; set; }
        public virtual EntityBranch EntityBranch { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int CreatedById { get; set; }
        public virtual User CreatedBy { get; set; }
        public int? ModifiedById { get; set; }
        public virtual User ModifiedBy { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
        public int ScopeOfWorkId { get; set; }
        public virtual ScopeOfWork ScopeOfWork { get; set; }
        public bool IsActive { get; set; } = true;        
    }
}