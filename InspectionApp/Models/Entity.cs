using System.Collections.Generic;
using InspectionApp.Repositories;

namespace InspectionApp.Models
{
    public class Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TradeLicenseNo { get; set; }
        public virtual ICollection<InspectionApplication> Applications { get; set; }
        public int OwnerId { get; set; }
        public virtual Contact Owner { get; set; }      
        public int CreatedById { get; set; }
        public virtual User CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<EntityBranch> EntityBranches { get; set; }
    }
}