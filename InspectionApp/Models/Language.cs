using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class Language
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}