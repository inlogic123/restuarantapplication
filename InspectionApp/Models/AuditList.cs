using System.Collections.Generic;

namespace InspectionApp.Models
{
    public class AuditList
    {
        public int Id { get; set; }
        public int Title { get; set; }
        public int StandardId { get; set; }
        public virtual Standard Standrad { get; set; }
        public virtual ICollection<AuditListItem> AuditListItems { get; set; }
    }
}