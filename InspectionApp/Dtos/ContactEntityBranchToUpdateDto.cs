namespace InspectionApp.Dtos
{
    public class ContactEntityBranchToUpdateDto
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public virtual ContactToCreateDto Contact { get; set; }
        public int EntityBranchId { get; set; }
    }
}