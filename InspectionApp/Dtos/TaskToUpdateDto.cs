using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class TaskToUpdateDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CreatedById { get; set; }
        // public int WorkFlowId { get; set; }
        public virtual ICollection<TaskActionToUpdateDto> TaskActions { get; set; }
    }
}