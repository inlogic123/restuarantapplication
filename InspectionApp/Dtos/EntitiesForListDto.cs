namespace InspectionApp.Dtos
{
    public class EntitiesForListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TradeLicenseNo { get; set; }
        public ContactForEntityListDto Owner { get; set; }
        public UserForEntityListDto CreatedBy { get; set; }
    }
}