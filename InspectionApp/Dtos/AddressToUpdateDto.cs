namespace InspectionApp.Dtos
{
    public class AddressToUpdateDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CountryId { get; set; }
        public int CityId { get; set; }
        public int AreaId { get; set; }
        public string Street { get; set; }
        public string BuildingNo { get; set; }
        public string MakaniNumber { get; set; }
        public string Long { get; set; }
        public string Lat { get; set; }
        public int EntityBranchId { get; set; }
        public bool IsActive { get; set; }
    }
}