using System;

namespace InspectionApp.Dtos
{
    public class ApplicationsToList
    {
        public int Id { get; set; }
        public string ApplicationNo { get; set; }
        public virtual LookupDto ApplicationStatus { get; set; }
        public EntityBranchDto EntityBranch { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual UserShortDto CreatedBy { get; set; }
        public virtual UserShortDto AssignedTo { get; set; }

    }
}