namespace InspectionApp.Dtos
{
    public class TaskToViewDto
    {
        public string Title { get; set; }
    }
}