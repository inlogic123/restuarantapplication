namespace InspectionApp.Dtos
{
    public class CompanyScopeOfWorkToCreateDto
    {
        public int EntityBranchId { get; set; }
        public int ScopeOfWorkId { get; set; }
    }
}