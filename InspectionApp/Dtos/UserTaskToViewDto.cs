namespace InspectionApp.Dtos
{
    public class UserTaskToViewDto
    {
        public int Id { get; set; }
        // public int AssignedToId { get; set; }
        // public virtual User AssignedTo { get; set; }
        // public int AssignedById { get; set; }
        // public virtual User AssignedBy { get; set; }
        public int TaskId { get; set; }
        public TaskToViewDto Task { get; set; }
        public int ApplicationId { get; set; }
        public ApplicationToViewDto Application { get; set; }
    }
}