namespace InspectionApp.Dtos
{
    public class EvaluationToViewDto
    {
        public int Id { get; set; }
        public string EvaluationNo { get; set; }
        public string EvaluationResult { get; set; }
        public ApplicationToViewDto Application { get; set; }
        public StandardToViewDto Standard { get; set; } 
    }
}