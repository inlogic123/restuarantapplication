namespace InspectionApp.Dtos
{
    public class UserToViewDto
    {
        public string UserName { get; set; }
    }
}