namespace InspectionApp.Dtos
{
    public class EvaluationToCreateDto
    {
        public string EvaluationNo { get; set; }
        public string EvaluationResult { get; set; }
        public int ApplicationId { get; set; }
        public int StandardId { get; set; } 
    }
}