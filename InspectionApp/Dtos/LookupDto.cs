namespace InspectionApp.Dtos
{
    public class LookupDto
    {
        public string Name { get; set; }
    }
}