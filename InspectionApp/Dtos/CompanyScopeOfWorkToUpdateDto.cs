namespace InspectionApp.Dtos
{
    public class CompanyScopeOfWorkToUpdateDto
    {
        public int Id { get; set; }
        public int EntityBranchId { get; set; }
        public int ScopeOfWorkId { get; set; }
    }
}