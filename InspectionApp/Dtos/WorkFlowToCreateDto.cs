using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class WorkFlowToCreateDto
    {
        public string Title { get; set; }
        public int LanguageId { get; set; }
        public virtual ICollection<TaskToCreateDto> Tasks { get; set; }
    }
}