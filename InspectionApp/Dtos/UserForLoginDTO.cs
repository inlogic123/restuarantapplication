namespace InspectionApp.Dtos
{
    public class UserForLoginDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}