namespace InspectionApp.Dtos
{
    public class EntityBranchAddressToCreate
    {
        public int EntityBranchId { get; set; }
        public int AddressId { get; set; }
        public AddressToCreateDto Address { get; set; }
    }
}