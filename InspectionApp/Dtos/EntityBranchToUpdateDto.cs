using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class EntityBranchToUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<EntityBranchAddressToUpdate> EntityBranchAddress { get; set; }
        public int EntityId { get; set; }
        public bool IsActive { get; set; } = true;
    }
}