using System;

namespace InspectionApp.Dtos
{
    public class ApplicationToUpdate
    {
        public int Id { get; set; }
        public string ApplicationNo { get; set; }
        public int ApplicationStatusId { get; set; }
        public int EntityBranchId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedById { get; set; }
        public int AssignedToId { get; set; }
    }
}