namespace InspectionApp.Dtos
{
    public class EntityBranchAddressToUpdate
    {
        public int Id { get; set; }
        public int EntityBranchId { get; set; }
        public int AddressId { get; set; }
        public AddressToUpdateDto Address { get; set; }
    }
}