using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class TasksToListDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public WorkflowToViewDto WorkFlow { get; set; }
        public UserToViewDto CreatedBy { get; set; }
        public UserToViewDto ModifiedBy { get; set; }
        public ICollection<TaskActionToViewDto> TaskActions { get; set; }
    }
}