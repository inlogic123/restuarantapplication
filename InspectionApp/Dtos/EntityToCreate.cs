using System.Collections.Generic;
using InspectionApp.Models;

namespace InspectionApp.Dtos
{
    public class EntityToCreate
    {
        public string Name { get; set; }
        public string TradeLicenseNo { get; set; }
        public OwnerToCreate Owner { get; set; }
        public int CreatedById { get; set; }
        public bool IsActive { get; set; } = true;
        public ICollection<EntityBranchToCreateDto> EntityBranches { get; set; }
    }
}