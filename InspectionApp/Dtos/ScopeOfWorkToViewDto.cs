namespace InspectionApp.Dtos
{
    public class ScopeOfWorkToViewDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}