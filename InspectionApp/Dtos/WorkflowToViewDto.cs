namespace InspectionApp.Dtos
{
    public class WorkflowToViewDto
    {
        public string Title { get; set; }
    }
}