namespace InspectionApp.Dtos
{
    public class TaskActionToUpdateDto
    {
        public int Id { get; set; }
        public string Action { get; set; }
        public int TaskId { get; set; }  
        // #nullable enable      
        // public int? NextTaskId { get; set; }               
        // public virtual TaskToUpdateDto? NextTask { get; set; }
        // #nullable disable
    }
}