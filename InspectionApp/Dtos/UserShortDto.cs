namespace InspectionApp.Dtos
{
    public class UserShortDto
    {
        public string UserName { get; set; }
    }
}