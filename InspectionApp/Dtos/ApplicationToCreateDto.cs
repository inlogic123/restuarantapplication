using System;

namespace InspectionApp.Dtos
{
    public class ApplicationToCreateDto
    {
        public string ApplicationNo { get; set; }
        // public int ApplicationStatusId { get; set; }
        public int EntityBranchId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedById { get; set; }
        public int ScopeOfWorkId { get; set; }

        public ApplicationToCreateDto()
        {
            CreatedDate = DateTime.Now;
        }
    }
}