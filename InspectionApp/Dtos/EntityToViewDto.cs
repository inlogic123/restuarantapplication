using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class EntityToViewDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<EntityBranchToViewDto> EntityBranches { get; set; }
    }
}