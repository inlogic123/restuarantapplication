namespace InspectionApp.Dtos
{
    public class EmployeeAreaScopeOfWorkDto
    {
        public int AreaId { get; set; }
        public int ScopeOfWorkId { get; set; }
    }
}