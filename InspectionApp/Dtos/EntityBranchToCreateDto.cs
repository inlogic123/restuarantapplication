
using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class EntityBranchToCreateDto
    {
        public string Name { get; set; }
         public AddressToCreateDto Address { get; set; }
        public ICollection<CompanyScopeOfWorkToCreateDto> CompanyScopeOfWorks { get; set; }
        public ICollection<ContactEntityBranchToCreateDto> ContactEntityBranches { get; set; }
        public int EntityId { get; set; }
        public bool IsActive { get; set; } = true;
    }
}