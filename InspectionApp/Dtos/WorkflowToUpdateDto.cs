using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class WorkflowToUpdateDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int LanguageId { get; set; }
        public virtual ICollection<TaskToUpdateDto> Tasks { get; set; }
    }
}