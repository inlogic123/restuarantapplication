using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class EntityToUpdateDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TradeLicenseNo { get; set; }
        public OwnerToUpdate Owner { get; set; }
        public int CreatedById { get; set; }
        public bool IsActive { get; set; } = true;
        public ICollection<EntityBranchToUpdateDto> EntityBranches { get; set; }
    }
}