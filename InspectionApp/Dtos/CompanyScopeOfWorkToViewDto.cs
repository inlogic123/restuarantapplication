namespace InspectionApp.Dtos
{
    public class CompanyScopeOfWorkToViewDto
    {
        public ScopeOfWorkToViewDto ScopeOfWork { get; set; }
    }
}