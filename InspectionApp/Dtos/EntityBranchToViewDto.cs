using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class EntityBranchToViewDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CompanyScopeOfWorkToViewDto> CompanyScopeOfWorks { get; set; }
    }
}