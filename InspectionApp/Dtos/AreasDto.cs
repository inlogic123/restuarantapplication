namespace InspectionApp.Dtos
{
    public class AreasDto
    {
        public int[] Areas { get; set; }
    }
}