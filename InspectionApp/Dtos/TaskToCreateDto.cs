using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class TaskToCreateDto
    {
        public string Title { get; set; }
        public int CreatedById { get; set; }
        // public int WorkFlowId { get; set; }
        public virtual ICollection<TaskActionToCreateDto> TaskActions { get; set; }
    }
}