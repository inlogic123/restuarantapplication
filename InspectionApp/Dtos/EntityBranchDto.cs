namespace InspectionApp.Dtos
{
    public class EntityBranchDto
    {
        public string Name { get; set; }
    }
}