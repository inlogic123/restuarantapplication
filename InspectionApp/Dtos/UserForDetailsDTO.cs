using System;
using System.Collections.Generic;

namespace InspectionApp.Dtos
{
    public class UserForDetailsDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PhotoUrl { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastActive { get; set; }
        public ICollection<UserTaskToViewDto> UserTasks { get; set; }
    }
}