namespace InspectionApp.Dtos
{
    public class ContactEntityBranchToCreateDto
    {
        public int ContactId { get; set; }
        public virtual ContactToCreateDto Contact { get; set; }
        public int EntityBranchId { get; set; }
    }
}