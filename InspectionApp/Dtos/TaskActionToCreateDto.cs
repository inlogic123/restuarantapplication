namespace InspectionApp.Dtos
{
    public class TaskActionToCreateDto
    {
        public string Action { get; set; }
        public int TaskId { get; set; }  
        // #nullable enable      
        // public int? NextTaskId { get; set; }               
        // public virtual TaskToCreateDto? NextTask { get; set; }
        // #nullable disable
    }
}