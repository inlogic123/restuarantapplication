namespace InspectionApp.Dtos
{
    public class TaskActionToViewDto
    {
        public string Action { get; set; }
    }
}