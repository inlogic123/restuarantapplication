using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Models;
using InspectionApp.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [AllowAnonymous]
    public class WorkFlowController : ControllerBase
    {
        private readonly IRepository<WorkFlow> _repository;
        private readonly IMapper _mapper;
        public WorkFlowController(IRepository<WorkFlow> repository, IMapper mapper)
        {
            _mapper = mapper;
            _repository = repository;
        }

        [HttpPost]
        public async Task<IActionResult> CreateWorkFlow(WorkFlowToCreateDto workFlowToCreateDto)
        {
            var workflow = _mapper.Map<WorkFlow>(workFlowToCreateDto);
            _repository.Create(workflow);

            if (!await _repository.SaveChanges())
                return BadRequest();

            return Ok();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateWorkFlow(int id, WorkflowToUpdateDto workflowToUpdate)
        {
            var workFlow = await _repository.GetById(id);
            var mappedWorkFlow = _mapper.Map(workflowToUpdate, workFlow);
            
            if (!await _repository.SaveChanges())
                return BadRequest();

            return Ok();
        }
    }
}