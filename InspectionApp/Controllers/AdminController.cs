using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAuditListRepository _auditListRepository;
        private readonly IMapper _mapper;
        private readonly IStandardRepository _standardRepository;
        public AdminController(IAuditListRepository auditListRepository, IStandardRepository standardRepository, IMapper mapper)
        {
            _standardRepository = standardRepository;
            _mapper = mapper;
            _auditListRepository = auditListRepository;
        }

        [HttpGet(Name = "GetStandardList")]
        public async Task<IActionResult> GetStandardList()
        {
            var standardList = await _standardRepository.GetAll();
            var standardListToReturn = _mapper.Map<IEnumerable<StandardListToViewDto>>(standardList);

            return Ok(standardListToReturn);
        }

        [HttpGet("GetStandardById/{id}")]
        public async Task<IActionResult> GetStandardById(int id)
        {
            var standard = await _standardRepository.GetById(id);
            var standardToReturn = _mapper.Map<StandardToViewDto>(standard);

            return Ok(standardToReturn);
        }

        [HttpPost(Name = "CreateStandard")]
        public async Task<IActionResult> CreateStandard(StandardForCreateDto standardForCreateDto)
        {
            var standard = _mapper.Map<Standard>(standardForCreateDto);
            _standardRepository.Create(standard);

            if (!await _standardRepository.SaveChanges())
                return BadRequest("Could not Create the Audit List");

            return Ok();
        }
        

        [HttpGet(Name = "GetAuditList")]
        public async Task<IActionResult> GetAuditLists()
        {
            var auditLists = await _auditListRepository.GetAll();
            var auditListsToReturn = _mapper.Map<IEnumerable<AuditListsToViewDto>>(auditLists);

            return Ok(auditListsToReturn);
        }

        [HttpPost(Name = "CreateAuditList")]
        public async Task<IActionResult> CreateAuditList(AuditListForCreateDto auditListForCreateDto)
        {
            var auditList = _mapper.Map<AuditList>(auditListForCreateDto);
            _auditListRepository.Create(auditList);

            if (!await _auditListRepository.SaveChanges())
                return BadRequest("Could not Create the Audit List");

            return Ok();
        }

        [HttpGet("GetAuditListById/{id}")]
        public async Task<IActionResult> GetAuditListById(int id)
        {
            var auditList = await _auditListRepository.GetById(id);
            var auditListToReturn = _mapper.Map<AuditListsToViewDto>(auditList);

            return Ok(auditListToReturn);
        }

    }
}