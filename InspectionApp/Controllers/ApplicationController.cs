using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Route("api/[Controller]/{userId}")]
    [ApiController]
    public class ApplicationController : ControllerBase
    {
        private readonly IApplicationRepository _applicationRepository;
        private readonly IMapper _mapper;
        public ApplicationController(
            IApplicationRepository applicationRepository,
            IMapper mapper)
        {
            _mapper = mapper;
            _applicationRepository = applicationRepository;
        }

        [HttpGet]
        public async Task<IActionResult> GetApplicationList(int userId, [FromQuery] UserParams userParams)
        {
            string role = User.FindFirst(ClaimTypes.Role).Value;

            var applications = await _applicationRepository.GetAllByRole(role, userId, userParams);

            var applicationsToReturn = _mapper.Map<IEnumerable<ApplicationsToList>>(applications);
            Response.AddPagination(applications.CurrentPage, applications.PageSize, applications.TotalCount, applications.TotalPages);

            return Ok(applicationsToReturn);
        }

        [HttpGet("{id}", Name = "GetApplicationById")]
        public async Task<IActionResult> GetApplicationById(int id)
        {
            var application = await _applicationRepository.GetById(id);
            var applicationToReturn = _mapper.Map<ApplicationForDetailsDto>(application);

            return Ok(applicationToReturn);
        }

        [HttpPost]
        public async Task<IActionResult> CreateApplication(int userId, ApplicationToCreateDto applicationToCreateDto)
        {
            var applicaiton = _mapper.Map<InspectionApplication>(applicationToCreateDto);
            _applicationRepository.Create(applicaiton);

            if (!await _applicationRepository.SaveChanges())
                return BadRequest("Could not Create an Application");

            if (!await CreateTask(applicaiton, userId))
                return BadRequest("Couldn't Create a Task");

            return Ok();
        }

        private async Task<bool> CreateTask(InspectionApplication application, int userId)
        {
            int inspectorId = await _applicationRepository.GetUserForTask(application.ScopeOfWorkId, 1);
            int taskId = await _applicationRepository.GetFirstTask();

            bool result = await _applicationRepository.CeateTask(new UserTask
            {
                ApplicationId = application.Id,
                AssignedToId = inspectorId,
                AssignedById = userId,
                TaskId = taskId,
                IsActive = true
            });

            if (!await _applicationRepository.SaveChanges())
                return false;

            return true;
        }

        [HttpPut]
        public async Task<IActionResult> UpdateApplication(ApplicationToUpdate applicationToUpdate)
        {
            var application = await _applicationRepository.GetById(applicationToUpdate.Id);
            _mapper.Map(applicationToUpdate, application);

            if (!await _applicationRepository.SaveChanges())
                return BadRequest();

            return Ok();
        }

        [HttpGet("GetEntities")]
        public async Task<IActionResult> GetEntities(int userId)
        {
            var areas = await _applicationRepository.GetUserAreaSOW(userId);
            var entities = await _applicationRepository.GetEntitiesByArea(areas);
            var entitiesToReturn = _mapper.Map<IEnumerable<EntityToViewDto>>(entities);
            
            return Ok(entitiesToReturn);
        }
    }
}