using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Models;
using InspectionApp.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class InspectionController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Evaluation> _repository;
        public InspectionController(IMapper mapper, IRepository<Evaluation> repository)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEvaluation(EvaluationToCreateDto evaluationToCreateDto)
        {
            var evaluation = _mapper.Map<Evaluation>(evaluationToCreateDto);
            _repository.Create(evaluation);

            if (!await _repository.SaveChanges())
                return BadRequest("Could not save the Evaluation");

            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetEvaluationList()
        {
            var evaluations = await _repository.GetAll();
            var evaluationsToReturn = _mapper.Map<EvaluationListToViewDto>(evaluations);

            return Ok(evaluationsToReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEvaluationById(int id)
        {
            var evaluation = await _repository.GetById(id);
            var evaluationToReturn = _mapper.Map<EvaluationToViewDto>(evaluation);

            return Ok(evaluationToReturn);
        }
    }
}