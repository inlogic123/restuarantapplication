using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Models;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Route("api/[Controller]/{userId}")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _repo;
        private readonly IMapper _mapper;
        public UsersController(IUserRepository repo, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = new User();
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            {
                user = await _repo.GetUser(id, false);
            }
            else
            {
                user = await _repo.GetUser(id, true);
            }

            var userToReturn = _mapper.Map<UserForDetailsDTO>(user);
            return Ok(userToReturn);
        }



        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserForUpdateDTO userForUpdateDTO)
        {
            if (id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var userFromRepo = await _repo.GetUser(id, true);
            _mapper.Map(userForUpdateDTO, userFromRepo);

            if (await _repo.SaveChanges())
                return NoContent();

            throw new Exception($"Updating user {id} failed on save");
        }
    }
}