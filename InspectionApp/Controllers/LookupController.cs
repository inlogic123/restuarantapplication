using System.Threading.Tasks;
using InspectionApp.Enums;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace InspectionApp.Controllers
{
    [AllowAnonymous]
    [ApiController]
    [Route("/api/[Controller]")]
    public class LookupController : ControllerBase
    {
        private readonly ILogger<LookupController> _logger;
        private readonly ILookupRepository _lookupRepository;

        public LookupController(ILogger<LookupController> logger, ILookupRepository lookupRepository)
        {
            _lookupRepository = lookupRepository;
            _logger = logger;
        }

        [HttpGet("getByKey/{key}/{Language}")]
        public async Task<IActionResult> GetLookupWithKey(string key, int language)
        {
            var lookupDetails = await _lookupRepository.GetLookupWithKey(key, (Languages)language);
            return Ok(lookupDetails);
        }

        [HttpGet("getByParent/{parentId}")]
        public async Task<IActionResult> GetLookupWithParent(int parentId)
        {
            var lookupDetails = await _lookupRepository.GetLookupWithParentId(parentId);
            return Ok(lookupDetails);
        }

        [HttpGet("getByParentAndLanguage/{parentId}/{languageId}")]
        public async Task<IActionResult> GetLookupWithParent(int parentId, int languageId)
        {
            var lookupDetails = await _lookupRepository.GetLookupWithParentAndLanguage(parentId, (Languages)languageId);
            return Ok(lookupDetails);
        }

    }
}