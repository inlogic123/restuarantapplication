using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Models;
using InspectionApp.Repositories;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [AllowAnonymous]
    [ServiceFilter(typeof(LogUserActivity))]
    [Route("api/[Controller]/{userId}")]
    [ApiController]
    public class EntityController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IEntityRepository _repository;
        public EntityController(IEntityRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<IActionResult> CreateEntity(int userId, EntityToCreate entityToCreate)
        {
            // if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            //     return Unauthorized();

            var entity = _mapper.Map<Entity>(entityToCreate);
            _repository.Create(entity);

            if (!await _repository.SaveChanges())
                return BadRequest();

            return Ok();
        }

        [HttpPut("id/{id}")]
        public async Task<IActionResult> UpdateEntity(int userId, int id, EntityToUpdateDto entityToUpdate)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var entity = await _repository.GetById(id);
            _mapper.Map(entityToUpdate, entity);
            
            if (!await _repository.SaveChanges())
                return BadRequest();

            return Ok();
        }

        [HttpGet]
        //  [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetEntities(int userId)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var entities = await _repository.GetEntities(userId);
            var entitiesToReturn = _mapper.Map<IEnumerable<EntitiesForListDto>>(entities);

            return Ok(entitiesToReturn);
        }

        [HttpGet("getById/{id}")]
        public async Task<IActionResult> GetEntityById(int id)
        {
            return Ok(await _repository.GetById(id));
        }

        [HttpGet("getByOwnerId/{ownerId}")]
        public async Task<IActionResult> GetEntityByOwnerId(int ownerId)
        {
            return Ok(await _repository.GetEntitiesByOwnerId(ownerId));
        }
    }
}