using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using InspectionApp.Dtos;
using InspectionApp.Helpers;
using InspectionApp.Repositories.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InspectionApp.Controllers
{
    [ApiController]
    [Route("api/[Controller]/{userId}")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IMapper _mapper;
        public TaskController(ITaskRepository taskRepository, IMapper mapper)
        {
            _mapper = mapper;
            _taskRepository = taskRepository;
        }

        // [HttpGet]
        // [Authorize(Roles = "Admin")]
        // public async Task<IActionResult> GetTaskList(int userId)
        // {
        //     if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
        //         return Unauthorized();

        //     var tasks = await _taskRepository.GetAll();
        //     var tasksToReturn = _mapper.Map<IEnumerable<TasksToListDto>>(tasks);

        //     return Ok(tasksToReturn);
        // }

        [HttpGet]
        [Authorize(Roles = "Admin,Inspector,User")]
        public async Task<IActionResult> GetUserTask(int userId,[FromQuery] UserParams userParams)
        {
            if (userId != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
                return Unauthorized();

            var tasks = await _taskRepository.GetUserTasks(userId, userParams);

            var tasksToReturn = _mapper.Map<IEnumerable<TasksToListDto>>(tasks);
            Response.AddPagination(tasks.CurrentPage, tasks.PageSize, tasks.TotalCount, tasks.TotalPages);

            return Ok(tasksToReturn);
        }
    }
}